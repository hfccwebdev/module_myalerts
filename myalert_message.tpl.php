<?php

/**
 * @file
 * myalert_message.tpl.php
 *
 * Template implementation to display a myalerts message.
 */
?>
<div id="<?php print $id; ?>" class="<?php print $classes; ?>">
  <span class="icon"></span>
  <?php if ($dismissible): ?>
    <span class="button"><button type="button">Mark item complete: </button></span>
  <?php endif; ?>
  <?php if ($link): ?>
    <a class="message" href="<?php print $link; ?>" tabindex="0"><?php print $content; ?></a>
  <?php else: ?>
    <?php print $content; ?>
  <?php endif; ?>
</div>
