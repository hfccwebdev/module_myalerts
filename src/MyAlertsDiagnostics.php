<?php

/**
 * Defines the MyAlertsDiagnostics class.
 */
class MyAlertsDiagnostics {

  /**
   * Implements hook_mysite_user_diagnotics_alter().
   */
  public static function alter(&$output, &$account) {
    if (!empty($account->field_user_hank_id)) {
      $hank_id = $account->field_user_hank_id[LANGUAGE_NONE][0]['safe_value'];
      $hank_info = HankTools::create($hank_id);
      self::showAlerts($output, $hank_info);
    }
  }

  /**
   * Add the alerts content to the output array.
   */
  public static function showAlerts(&$output, &$hank_info) {
    $alerts = MyAlertService::create($hank_info->formatId())->getBlockContent();
    $output['mysite_alerts'] = [
      'title' => ['#prefix' => '<h2>', '#markup' => t('My Alerts'), '#suffix' => '</h2>'],
      'alerts' => !empty($alerts) ? $alerts : ['#markup' => t('<p>No alerts to display.</p>')],
    ];
  }
}
