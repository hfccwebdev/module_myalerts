<?php

/**
 * Defines the base alert handler interface.
 */
interface MyAlertBaseHandlerInterface {

  /**
   * Instantiates a new object of this class.
   *
   * @param HankTools $hanktools
   *   The HankTools object for the PERSON.
   * @param StdClass $alert_type
   *   The Alert Type to handle.
   */
  public static function create(HankTools $hanktools, StdClass $alert_type);

  /**
   * Process all alerts of the current type.
   *
   * @return MyAlerts[]
   *    An array of currently active alerts.
   */
  public function processAlertType();

  /**
   * Check completion status for an alert.
   *
   * @param EntityInterface $alert
   *   The alert being handled.
   *
   * @return bool
   *   Returns TRUE if the alert is active.
   */
  public function checkAlertStatus(EntityInterface $alert);

}

/**
 * Defines the base alert handler class.
 */
class MyAlertBaseHandler implements MyAlertBaseHandlerInterface {

  /**
   * Stores the HankTools Object.
   *
   * @var HankTools
   */
  protected $hanktools;

  /**
   * Stores the alert type.
   *
   * @var StdClass;
   */
  protected $alert_type;

  /**
   * {@inheritdoc}
   */
  public static function create(HankTools $hanktools, StdClass $alert_type) {
    return new static($hanktools, $alert_type);
  }

  /**
   * The class constructor
   *
   * @param HankTools $hanktools
   *   The HankTools object for the PERSON.
   * @param StdClass $alert_type
   *   The Alert Type to handle.
   */
  public function __construct(HankTools $hanktools, StdClass $alert_type) {
    $this->hanktools = $hanktools;
    $this->alert_type = $alert_type;
  }

  /**
   * {@inheritdoc}
   */
  public function processAlertType() {
    return $this->isRepeatable() ? $this->processRepeatableAlerts() : $this->processSingularAlerts();
  }

  /**
   * Process repeatable alerts.
   *
   * @return MyAlerts[]
   *    An array of currently active alerts.
   */
  private function processRepeatableAlerts() {

    $active_alerts = [];
    $alerts = $this->getAlertsByType();

    // Save the deltas for existing alerts so that we can compare them to new possible alerts.

    $deltas = [];
    foreach ($alerts as $alert) {
      if (!empty($alert->delta)) {
        $deltas[$alert->delta] = $alert->id();
      }
      else {
        watchdog('myalerts', 'Repeatable alert %id has missing delta value.', ['%id' => $alert->id()], WATCHDOG_ERROR);
      }
    }

    // Merge any possible new alerts into the array.

    if ($possible_alerts = $this->getRepeatableAlertData()) {
      foreach ($possible_alerts as $alert) {
        if (empty($deltas[$alert['delta']])) {
          $new_alert = entity_create("myalert", [
            "hank_id" => $this->hanktools->id(),
            "machine_name" => $this->alert_type->machine_name,
          ] + $alert);
          $alerts[] = $new_alert;
        }
      }
    }

    // Now check status on all alerts.

    foreach ($alerts as $alert) {
      if ($this->checkAlertStatus($alert)) {
        if ($alert->needs_update) {
          $alert->save();
        }
        $active_alerts[$alert->id()] = $alert;
      }
    }

    return $active_alerts;
  }

  /**
   * Process non-repeatable alerts.
   *
   * @return MyAlerts[]
   *    An array of currently active alerts.
   */
  private function processSingularAlerts() {

    $active_alerts = [];

    if ($existing_alerts = $this->getAlertsByType()) {
      $alert = reset($existing_alerts);
    }
    else {
      $alert = entity_create("myalert", [
        "hank_id" => $this->hanktools->id(),
        "machine_name" => $this->alert_type->machine_name,
      ]);
    }

    // Check the alert here.
    if ($this->checkAlertStatus($alert)) {
      if ($alert->needs_update) {
        $alert->save();
      }
      $active_alerts[$alert->id()] = $alert;
    }

    return $active_alerts;
  }

  /**
   * Determine if this alert type supports repeating.
   */
  private function isRepeatable() {
    return !empty($this->alert_type->repeats) && ($this->alert_type->repeats !== 'none');
  }

  /**
   * Get alerts for a person by type.
   */
  protected function getAlertsByType() {

    $query = db_select('myalerts', 'm');
    $query->fields('m', ['alert_id']);
    $query->condition('hank_id', intval($this->hanktools->id()));
    $query->condition('machine_name', $this->alert_type->machine_name);
    $query->orderBy('created');

    if ($result = $query->execute()->fetchAllAssoc('alert_id')) {
      return entity_load('myalert', array_keys($result));
    }
    else {
      return [];
    }
  }

  /**
   * Load data required to generate repeatable alerts.
   *
   * This method should be implemented in sub-classes or it may not work!
   *
   * @return array
   *    An array of alert data.
   */
  protected function getRepeatableAlertData() {

    switch ($this->alert_type->repeats) {
      case("annual"):
        $year = date('Y', REQUEST_TIME);
        return [['delta' => $year]];
      case("semester"):
        $term_id = WebServicesClient::getCurrentTerm();
        $term_name = WebServicesClient::getCurrentTerm();
        return [['delta' => $term_id]];
      default:
        // All other repeatable types need a custom handler to set deltas.
        watchdog(
          'myalerts',
          'No method defined to load repeatable alert data for alert type %id.',
          ['%id' => $this->alert_type->machine_name],
          WATCHDOG_ERROR
        );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkAlertStatus(EntityInterface $alert) {
    if ($alert->completed || $alert->dismissed || $alert->expired) {
      return FALSE;
    }
    elseif (isset($alert->valid_delta) && ($alert->delta !== $alert->valid_delta)) {
      // Hide invalid alerts.
      return FALSE;
    }
    elseif (isset($this->alert_type->end_date) && (REQUEST_TIME > $this->alert_type->end_date)) {
      // @todo: Add additional handling for instance end dates on repeatable alerts.
      $alert->expired = TRUE;
      $alert->save();
      return FALSE;
    }
    else {
      return TRUE;
    }
  }
}
