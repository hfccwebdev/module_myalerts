<?php

/**
 * Defines the enrollment steps alert handler.
 */
class MyAlertStepsHandler extends MyAlertBaseHandler {

  /**
   * Stores the checklist values.
   */
  private $checklist;

  /**
   * {@inheritdoc}
   */
  public function checkAlertStatus(EntityInterface $alert) {

    // Do not check parent::checkAlertStatus() for these alerts.
    // These alerts can go from completed back to incomplete at any time.
    if ($this->checklist = $this->hanktools->getStudentChecklist()) {
      switch ($alert->machine_name) {
        case 'des_orientation':
          return $this->checkAlertFlag($alert, 'DES_ORIENTATION');
        case 'step2documents':
          return $this->checkAlertFlagOrRegRecord($alert, 'DOCUMENTS');
        case 'step3placementmath':
          return $alert->dismissed ? FALSE : $this->checkAlertFlag($alert, 'PLACEMENT_MATH');
        case 'step3placementengl':
          return $alert->dismissed ? FALSE : $this->checkAlertFlag($alert, 'PLACEMENT_ENGL');
        case 'step4orientation':
          return $this->checkAlertFlag($alert, 'ORIENTATION');
        case 'step5advising':
          return $alert->dismissed ? FALSE : $this->checkAlertFlagOrRegRecord($alert, 'ADVISING');
        case 'step6register':
          return $this->checkAlertFlag($alert, 'REG_RECORD');
        case 'step7payment':
          return $this->checkAlertFlag($alert, 'PAYMENT');
        case 'addressconfirm':
          return $this->checkAlertFlag($alert, 'ADDRESS_CONFIRMED');
        case 'agreementconfirm':
          return $this->checkAlertFlag($alert, 'AGREEMENTS_CONFIRMED');
      }
    }
  }

  /**
   * Check alert value or student registration.
   */
  private function checkAlertFlagOrRegRecord(EntityInterface $alert, $attribute) {
    if ($this->checklist->{$attribute} == 'X') {
      return FALSE;
    }
    $alert->completed = ($this->checklist->{$attribute} == 'Y' || $this->checklist->REG_RECORD == 'Y');
    $alert->conditionalSave();
    return !$alert->completed;
  }

  /**
   * Check alert status.
   */
  private function checkAlertFlag(EntityInterface $alert, $attribute) {
    if ($this->checklist->{$attribute} == 'X') {
      return FALSE;
    }
    $alert->completed = ($this->checklist->{$attribute} == 'Y');
    $alert->conditionalSave();
    return !$alert->completed;
  }
}
