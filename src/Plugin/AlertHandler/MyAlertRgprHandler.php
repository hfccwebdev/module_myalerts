<?php

/**
 * Defines the priority registration alert handler.
 */
class MyAlertRgprHandler extends MyAlertBaseHandler {

  /**
   * Stores priority registration info so we can reuse it to check existing alerts.
   */
  private $rgprData;

  /**
   * @inheritdoc
   */
  protected function getRepeatableAlertData() {
    if ($data = $this->hanktools->getPriorityRegistration()) {
      $output = [];
      foreach ($data as $rgpr) {
        $output[$rgpr->RGPR_TERM] = [
          'delta' => $rgpr->RGPR_TERM,
          'data' => [
            "@myterm" => $rgpr->RGPR_TERM,
            "@regstart" => format_date(WebServicesClient::getTerms()[$rgpr->RGPR_TERM]["term_reg_start_date"], "custom", "m/d/Y"),
            "@mydate" => $rgpr->RGPR_START_DATE,
            "@mytime" => $rgpr->RGPR_START_TIME,
          ],
        ];
      }
      $this->rgprData = $output;
      return $output;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkAlertStatus(EntityInterface $alert) {
    if (parent::checkAlertStatus($alert)) {

      // Expire this alert if no corresponding data found in the API response.

      if (!isset($this->rgprData[$alert->delta])) {
        $alert->expired = TRUE;
        $alert->save();
        return FALSE;
      }

      // Mark completed if student has registered for classes in $alert->delta term.
      // Set expired if current date > reg start date + 2 weeks.

      $term = WebServicesClient::getTerms()[$alert->delta];
      $schedule = $this->hanktools->getStudentSchedule();

      if (!empty($schedule)) {
        foreach ($schedule as $class) {
          if ($class->STC_TERM == $alert->delta) {
            $alert->completed = TRUE;
            $alert->save();
            return FALSE;
          }
        }
      }

      $cutoff_time = strtotime($alert->data['@mydate'] . " " . $alert->data['@mytime']) + 86400 * 7;
      if (REQUEST_TIME > $cutoff_time) {
        $alert->expired = TRUE;
        $alert->save();
        return FALSE;
      }

      $rgprtime = $this->rgprData[$alert->delta]['data']['@mydate'] . " " . $this->rgprData[$alert->delta]['data']['@mytime'];
      $alertime = $alert->data['@mydate'] . " " . $alert->data['@mytime'];

      if ($rgprtime !== $alertime) {
        $alert->data['@mydate'] = $this->rgprData[$alert->delta]['data']['@mydate'];
        $alert->data['@mytime'] = $this->rgprData[$alert->delta]['data']['@mytime'];
        $alert->save();
      }

      return TRUE;
    }
  }
}
