<?php

/**
 * Defines the Student ID card handler.
 */
class MyAlertIdHandler extends MyAlertBaseHandler {

  /**
   * SHA1 hash of default HANK ID Photo
   */
  const DEFAULT_PHOTO = 'cdb4657f423f6af5e1cb79e16747584d221694cd';

  /**
   * {@inheritdoc}
   */
  public function checkAlertStatus(EntityInterface $alert) {
    if ($this->hanktools->isUndergrad()) {
      if ($photo = $this->hanktools->getIdPhoto()) {
        if (sha1($photo) !== self::DEFAULT_PHOTO) {
          $alert->completed = TRUE;
          $alert->save();
          return FALSE;
        }
      }
      return TRUE;
    }
  }
}
