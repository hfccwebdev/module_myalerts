<?php

/**
 * Defines the priority registration alert handler.
 */
class MyAlertFaHandler extends MyAlertBaseHandler {

  /**
   * Stores parsed data from the HANK API for use by checkAlertStatus().
   */
  private $fa_checklist_flags;

  /**
   * @inheritdoc
   */
  protected function getRepeatableAlertData() {
    if ($checklist = $this->hanktools->getStudentChecklist()) {
      $output = [];
      if (intval($checklist->FA_YEAR1) > 0 && isset($checklist->FA_FLAG1)) {
        $output[] = [
          'delta' => $checklist->FA_YEAR1,
          'data' => ["@acyr" => $this->hanktools->formatAcYr($checklist->FA_YEAR1)],
        ];
        $this->fa_checklist_flags[$checklist->FA_YEAR1] = $checklist->FA_FLAG1;
      }
      if (intval($checklist->FA_YEAR2) > 0 && isset($checklist->FA_FLAG2)) {
        $output[] = [
          'delta' => $checklist->FA_YEAR2,
          'data' => ["@acyr" => $this->hanktools->formatAcYr($checklist->FA_YEAR2)],
        ];
        $this->fa_checklist_flags[$checklist->FA_YEAR2] = $checklist->FA_FLAG2;
      }
      return $output;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkAlertStatus(EntityInterface $alert) {

    // Do not check parent::checkAlertStatus() for these alerts.
    // These alerts can go from completed back to incomplete at any time.
    if ($alert->expired) {
      return FALSE;
    }
    elseif (intval($alert->delta) < WebServicesClient::getCurrentAcYr()) {
      $alert->expired = TRUE;
      $alert->save();
      return FALSE;
    }
    elseif (isset($this->fa_checklist_flags[$alert->delta])) {
      $flagged = ($this->fa_checklist_flags[$alert->delta] == 'Y');

      // Only save new alerts if flag is set.
      if ($flagged || $alert->id()) {
        $alert->completed = !$flagged;
        $alert->save();
      }

      return $flagged;
    }
    else {
      // Actual HANK status is unknown. Do not display.
      return FALSE;
    }
  }
}
