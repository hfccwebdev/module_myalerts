<?php

/**
 * Defines the priority registration alert handler.
 */
class MyAlertFafsaHandler extends MyAlertBaseHandler {

  /**
   * Stores parsed data from the HANK API for use by checkAlertStatus().
   */
  private $fafsa_submitted;

  /**
   * @inheritdoc
   */
  protected function getRepeatableAlertData() {
    if ($checklist = $this->hanktools->getStudentChecklist()) {
      $output = [];
      if (intval($checklist->FA_YEAR1) > 0 && isset($checklist->FAFSA_SUBMITTED1)) {
        $output[] = [
          'delta' => $checklist->FA_YEAR1,
          'data' => ["@acyr" => $this->hanktools->formatAcYr($checklist->FA_YEAR1)],
        ];
        $this->fafsa_submitted[$checklist->FA_YEAR1] = $checklist->FAFSA_SUBMITTED1;
      }
      if (intval($checklist->FA_YEAR2) > 0 && isset($checklist->FAFSA_SUBMITTED2)) {
        $output[] = [
          'delta' => $checklist->FA_YEAR2,
          'data' => ["@acyr" => $this->hanktools->formatAcYr($checklist->FA_YEAR2)],
        ];
        $this->fafsa_submitted[$checklist->FA_YEAR2] = $checklist->FAFSA_SUBMITTED2;
      }
      return $output;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkAlertStatus(EntityInterface $alert) {
    if (!parent::checkAlertStatus($alert)) {
      return FALSE;
    }
    elseif (intval($alert->delta) < WebServicesClient::getCurrentAcYr()) {
      $alert->expired = TRUE;
      $alert->save();
      return FALSE;
    }
    elseif (isset($this->fafsa_submitted[$alert->delta])) {
      $alert->completed = ($this->fafsa_submitted[$alert->delta] == 'Y');
      $alert->save();
      return !$alert->completed;
    }
    else {
      // Actual HANK status is unknown. Do not display.
      return FALSE;
    }
  }
}
