<?php

/**
 * Defines the Alert Block.
 */
class MyAlertBlock extends HankBlockBase {

  /**
   * Returns values for hook_block_info().
   */
  public function info() {
    return [
      'info' => t('My Alerts'),
      'cache' => DRUPAL_NO_CACHE,
    ];
  }

  /**
   * Returns the block label.
   */
  public function label() {
    return t('My Alerts');
  }

  /**
   * Build content for this block.
   */
  protected function build(&$output) {
    parent::build($output);
    if ($content = MyAlertService::create($this->person->hank_id)->getBlockContent()) {
      $output += $content;
    }
  }
}
