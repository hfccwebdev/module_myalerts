<?php

/**
 * Defines the MyAlerts service interface.
 */
interface MyAlertServiceInterface {

  /**
   * Instantiates a new object of this class.
   *
   * @param string $hank_id
   *   The HANK ID of the PERSON.
   */
  public static function create($hank_id = NULL);

  /**
   * Returns an array of available alert types.
   *
   * @return \StdClass[]
   *   An array of available alert type objects loaded from the alert_types.json configuration file.
   *
   *   Objects must contain the following properties:
   *   - machine_name: The machine name of the alert type.
   *   - title: A title describing the alert type. (Not shown to end users.)
   *   - description: The message to show to users. May contain format_string() replacement tokens.
   *   - link: Target link. (Must be defined, but may be an empty string.)
   *   - handler: The alert handler class name.
   *   - level: Alert level, implemented in CSS overrides.
   *       - 2: Default (warning) icon and colors.
   *       - 1: Urgent (stop) icon and colors.
   *   - dismissible: boolean, alert may be dismissed by user if true.
   *
   *   Objects may contain the following properties:
   *   - audience: target audience for alert, such as admitted, student, or employee.
   *   - start_date: Unix timestamp. Do not show alert before date.
   *   - end_date: Unix timestamp. Expire and do not show alert after date.
   *   - prereq: Do not show if prereq alert(s) are not completed.
   *   - overrides: If alert is active, do not show overridden alerts.
   *   - repeats: Repeatable alerts, specify interval.
   *       - semester: Repeats every semester, stores semester ID as delta.
   *       - annual: Repeats every year, stores year as delta.
   *       - others could be used with additional custom code.
   *   - valid_delta: Only show repeating alerts if current delta matches specified value.
   */
  public static function getAlerts();

  /**
   * Returns properties for a single alert type.
   *
   * @param string $machine_name
   *   The name of the alert type to read.
   *
   * @return \StdClass
   *   The requested alert properties.
   */
  public static function getAlert($machine_name);

  /**
   * Check all current alerts.
   */
  public function checkAll();

  /**
   * Load alerts for the current HankTools Person.
   *
   * @param string $machine_name
   *   If set, filter results by this machine name.
   *
   * @return MyAlerts[]
   *   An array of matching alerts.
   */
  public function getPersonAlerts($machine_name = NULL);

  /**
   * Get block contents.
   *
   * @return string[]
   *   A renderable array.
   */
  public function getBlockContent();

}

/**
 * Defines the MyAlerts service class.
 */
class MyAlertService implements MyAlertServiceInterface {

  /**
   * Stores the alert types array.
   */
  protected static $alert_types;

  /**
   * Stores the HankTools Object.
   *
   * @var HankTools
   */
  protected $hanktools;

  /**
   * {@inheritdoc}
   */
  public static function create($hank_id = NULL) {
    return new static($hank_id);
  }

  /**
   * The class constructor.
   */
  public function __construct($hank_id = NULL) {
    $this->hanktools = HankTools::create($hank_id);
  }

  /**
   * {@inheritdoc}
   */
  public static function getAlerts() {
    if (static::$alert_types === NULL) {
      $path = drupal_get_path("module", "myalerts") . "/config/alert_types.json";
      $file = file_get_contents($path);
      static::$alert_types = (array) json_decode($file);
      if (empty(static::$alert_types)) {
        watchdog('myalerts', 'Cannot read myalerts configuration file.', [], WATCHDOG_CRITICAL);
      }
    }
    return static::$alert_types;
  }

  /**
   * {@inheritdoc}
   */
  public static function getAlert($machine_name) {
    $alerts = static::getAlerts();
    return !empty($alerts[$machine_name]) ? $alerts[$machine_name] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function checkAll() {
    $active_alerts = [];
    foreach (static::getAlerts() as $type) {
      // @todo: Move audience checking here for employee/faculty alerts to make sense.
      $handler_class = !empty($type->handler) ? $type->handler : 'MyAlertBaseHandler';
      if (class_exists($handler_class)) {

        // Now check for reasons to skip this alert type.
        $skip = FALSE;

        if(isset($type->start_date) && ($type->start_date > REQUEST_TIME)) {
          $skip = TRUE;
        }

        if(isset($type->prereq)) {
          foreach ($type->prereq as $prereq) {
            if(!$this->checkPrereq($prereq)) {
              $skip = TRUE;
            }
          }
        }

        if (!$skip) {
          $active_alerts += $handler_class::create($this->hanktools, $type)->processAlertType();
        }
      }
      else {
        drupal_set_message(t('Cannot find handler %c for alert type %t', ['%c' =>$handler_class, '%t' => $type->machine_name]), 'error');
      }
    }
    return $active_alerts;
  }

  /**
   * Check prerequisite alert status.
   *
   * Note: this does not run a full test on the prerequisite alert.
   * It simply checks for an existing alert record in the database
   * that has a completed or dismissed state. (It does not check
   * for expired alerts, either.)
   *
   * @param string $machine_name
   *   The machine name of the prerequisite to check.
   *
   * @return bool
   *   Returns TRUE if the prerequite was satisified
   *   (alert has been completed or dismissed).
   */
  protected function checkPrereq($machine_name) {
    if ($alerts = $this->getPersonAlerts($machine_name)) {
      foreach ($alerts as $alert) {
        return ($alert->completed || $alert->dismissed);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPersonAlerts($machine_name = NULL) {
    $query = db_select('myalerts', 'm');
    $query->fields('m', ['alert_id']);
    $query->condition('hank_id', intval($this->hanktools->id()));
    if (!empty($machine_name)) {
      $query->condition('machine_name', $machine_name);
    }
    $query->orderBy('created');

    if ($result = $query->execute()->fetchAllAssoc('alert_id')) {
      return entity_load('myalert', array_keys($result));
    }
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getBlockContent() {

    $alerts = $this->checkAll();
    $output = [];
    foreach ($alerts as $alert) {

      $classes = [
        'myalerts-item',
        drupal_html_class("myalerts-{$alert->machine_name}"),
        drupal_html_class("level{$alert->level}"),
      ];

      if ($alert->dismissible) {
        $classes[] = "dismissible";
      }

      $output["myalert{$alert->id()}"] = [
        '#theme' => 'myalert_message',
        '#alert_id' => $alert->id(),
        '#machine_name' => $alert->machine_name,
        '#title' => $alert->title,
        '#message' => t($alert->description, (!empty($alert->data) ? $alert->data : [])),
        '#link' => $alert->link,
        '#dismissible' => $alert->dismissible,
        '#classes' => $classes,
      ];
    }
    return $output;
  }
}
