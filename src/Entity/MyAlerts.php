<?php

/**
 * The MyAlerts interface.
 *
 * Note: Interfaces must be in the same file as their classes
 * for Drupal 7's autoloader to find them.
 *
 */
interface MyAlertsInterface {

  /**
   * Gets the identifier using Drupal 8 syntax.
   *
   * @return int
   *   The Entity ID.
   */
  public function id();

  /**
   * Conditional save.
   *
   * Saves the alert only if it is not marked completed or not new.
   */
  public function conditionalSave();

}

/**
 * The MyAlerts entity class.
 */
class MyAlerts extends Entity implements MyAlertsInterface {

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->identifier();
  }

  /**
   * {@inheritdoc}
   */
  public function conditionalSave() {
    if (!$this->completed || empty($this->is_new)) {
      return $this->save();
    }
  }
}
