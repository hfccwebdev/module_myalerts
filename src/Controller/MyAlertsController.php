<?php

/**
 * The EntityAPIController extension for MyAlerts entities.
 */
class MyAlertsController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = []) {
    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

  /**
   * {@inheritdoc}
   */
  public function create(array $values = []) {
    if (!isset($values['machine_name'])) {
      drupal_set_message(t('MyAlert entities <em>must</em> specify a machine_name on creation.'), 'error');
      return NULL;
    }
    if ($type = MyAlertService::getAlert($values['machine_name'])) {
      $values += (array) $type + [
        'created' => REQUEST_TIME,
        'updated' => REQUEST_TIME,
        'needs_update' => TRUE,
        'completed' => NULL,
        'dismissed' => NULL,
        'expired' => NULL,
      ];
      return parent::create($values);
    }
    else {
      drupal_set_message(t('Cannot create MyAlert of type %m', ['%m' => $values['machine_name']]), 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function attachLoad(&$queried_entities, $revision_id = FALSE) {
    foreach ($queried_entities as $entity) {
      if ($type = MyAlertService::getAlert($entity->machine_name)) {
        foreach ((array) $type as $key => $value) {
          $entity->$key = $value;
        }
      }
      $entity->needs_update = FALSE;
    }
    return parent::attachLoad($queried_entities, $revision_id);
  }

  /**
   * {@inheritdoc}
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $entity->updated = REQUEST_TIME;
    parent::save($entity, $transaction);
    $entity->needs_update = FALSE;
  }
}
