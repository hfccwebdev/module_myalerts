<?php

/**
 * My Alerts API Interface.
 */
interface MyAlertsApiInterface {

  /**
   * Dismiss an alert.
   *
   * @param MyAlerts $alert
   */
  public static function dismiss($alert);

  /**
   * Enable an alert.
   *
   * @param MyAlerts $alert
   */
  public static function enable($alert);
}

/**
 * My Alerts API.
 */
class MyAlertsApi {

  /**
   * {@inheritdoc}
   */
  public static function dismiss($alert) {
    if ($alert->dismissed) {
      // Don't do anything, just return OK response.
      drupal_json_output(['response' => 'DISMISSED']);
      drupal_exit();
    }
    elseif ($alert->dismissible) {
      $alert->dismissed = TRUE;
      $alert->save();
      drupal_json_output(['response' => 'DISMISSED']);
      drupal_exit();
    }
    else {
      drupal_json_output(['response' => 'ERROR: not dismissible']);
      drupal_exit();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function enable($alert) {
    if (!$alert->dismissed && !$alert->completed) {
      // Don't do anything, just return OK response.
      drupal_json_output(['response' => 'ENABLED']);
      drupal_exit();
    }
    elseif ($alert->dismissible) {
      $alert->dismissed = FALSE;
      $alert->save();
      drupal_json_output(['response' => 'ENABLED']);
      drupal_exit();
    }
    else {
      drupal_json_output(['response' => 'ERROR: cannot be enabled']);
      drupal_exit();
    }
  }
}
