<?php

/**
 * Defines the My Alerts Page Controller.
 */
class MyAlertsPageController {

  public static function view($user) {
    if (empty($user->field_user_hank_id)) {
      return ['#markup' => t('Could not find HANK information for the specified user.')];
    }
    $hank_id = $user->field_user_hank_id[LANGUAGE_NONE][0]['safe_value'];
    $hank_info = HankTools::create($hank_id);
    $alert_service = MyAlertService::create($hank_info->formatId());
    $alerts = $alert_service->getPersonAlerts();
    return [];
  }
}
