<?php

/**
 * @file
 * Contains theme functions for My Alerts module.
 *
 * @see myalerts.module
 */

function template_preprocess_myalert_message(&$variables) {
  $element = $variables['element'];
  $variables['classes_array'] = $element['#classes'];
  $variables['classes'] = implode(' ', ($element['#classes']));
  $variables['dismissible'] = $element['#dismissible'];
  $variables['id'] = 'myalerts-' . $element['#alert_id'];
  $variables['content'] = $element['#message'];
  $variables['link'] = $element['#link'];
}
