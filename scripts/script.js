/**
 * @file
 * Provides dismissible alert toggling for the myalerts module.
 */
jQuery(document).ready(function($){

  /**
   * Sets styles and button tooltip based on API results.
   *
   * @param classList
   *   The classes on the alert's wrapper div.
   *
   * @param {string} message
   *   The button tooltip text.
   *
   * @param {string} status
   *   The status returned by the myalerts API ajax call.
   *   - ENABLED: The alert dismissed value was set to FALSE.
   *   - DISMISSED: The alert dismissed value was set to TRUE.
   */
  function setStyles(classList, message, status) {
    switch (status) {
      case 'ENABLED' :
        message = 'Mark item as complete: ';
        classList.remove('dismissed', 'completed');
        classList.add('dismissible');
        break;
      case 'DISMISSED' :
        message = 'Undo completed item: ';
        classList.remove('dismissible');
        classList.add('dismissed', 'completed');
        break;
      default :
        return
    }
  }

  $('.myalerts-item button').click(function(){
    var alert = $(this).parent().parent();
    var message = $(this).text();
    var id = alert.attr('id').substr(9);
    var classList = alert.attr('classList');
    var callbackUrl;

    classList.contains('completed') && (callbackUrl = '/api/myalerts/enable/' + id);
    classList.contains('dismissible') && (callbackUrl = '/api/myalerts/dismiss/' + id);

    var result = $.ajax({
      url: callbackUrl,
      cache: false,
      success: function (data) {
        setStyles(classList, message, data.response);
      },
      error: function (data) {
        console.log(callbackUrl + ' error ' + data.status + ': ' + data.statusText);
      }
    });
  })

})
